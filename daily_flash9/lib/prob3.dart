import 'package:flutter/material.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash | Prob3"),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.builder(
            itemCount: 10,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: const Color.fromARGB(112, 0, 0, 0), width: 2),
                      //borderRadius: const BorderRadius.all(Radius.circular(10)
                    ),
                    child: Row(
                      children: [
                        Expanded(flex: 1, child: Container()),
                        Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 2,
                                      color:
                                          const Color.fromARGB(112, 0, 0, 0)),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(15))),
                              child: Image.network(
                                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQn0bE-GiPrQhyZJis83z7rKVNMMolGTg2rSw&usqp=CAU",
                                height: 80,
                                width: 100,
                                fit: BoxFit.fill,
                              ),
                            )
                          ],
                        ),
                        Expanded(flex: 1, child: Container()),
                        Column(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: 70,
                              width: 150,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 2,
                                      color:
                                          const Color.fromARGB(112, 0, 0, 0)),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20))),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              child: const Text("Core2Web"),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              height: 70,
                              width: 150,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 2,
                                      color:
                                          const Color.fromARGB(112, 0, 0, 0)),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20))),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              child: const Text("Biencaps"),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              height: 70,
                              width: 150,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 2,
                                      color:
                                          const Color.fromARGB(112, 0, 0, 0)),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20))),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              child: const Text("Incubator"),
                            ),
                          ],
                        ),
                        Expanded(flex: 1, child: Container()),
                        const Expanded(
                            flex: 3,
                            child: Icon(
                              Icons.check_circle_outlined,
                              size: 50,
                              color: Color.fromARGB(112, 0, 0, 0),
                            )),
                        Expanded(flex: 1, child: Container()),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  )
                ],
              );
            }),
      ),
    );
  }
}
