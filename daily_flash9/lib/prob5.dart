import 'package:flutter/material.dart';

class Prob5 extends StatefulWidget {
  const Prob5({super.key});

  @override
  State createState() => _Prob5State();
}

class _Prob5State extends State {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  bool clicked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob 5"),
        backgroundColor: Colors.blue,
      ),
      body: Form(
        key: formKey,
        child: Column(
          children: [
            Expanded(flex: 10, child: Container()),
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                hintText: "Enter Name",
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.purple.shade100,
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Please Enter value";
                } else {
                  return null;
                }
              },
            ),
            Expanded(flex: 2, child: Container()),
            TextFormField(
              controller: phoneController,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: "Enter Phone Number",
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.purple.shade100,
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Please Enter value";
                } else {
                  return null;
                }
              },
            ),
            Expanded(flex: 2, child: Container()),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (clicked) {
                      clicked = false;
                    } else {
                      clicked = true;
                    }
                  });
                },
                child: Text((clicked) ? "Restart" : "Submit")),
            Expanded(flex: 10, child: Container()),
            (clicked)
                ? SizedBox(
                    child: Column(children: [
                      Text(nameController.text),
                      Text(phoneController.text)
                    ]),
                  )
                : Container(),
            Expanded(flex: 10, child: Container()),
          ],
        ),
      ),
    );
  }
}
