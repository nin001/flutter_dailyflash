import 'package:daily_flash9/prob1.dart';
import 'package:daily_flash9/prob2.dart';
import 'package:daily_flash9/prob3.dart';
import 'package:daily_flash9/prob4.dart';
import 'package:daily_flash9/prob5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(debugShowCheckedModeBanner: false, home: Prob5());
  }
}
