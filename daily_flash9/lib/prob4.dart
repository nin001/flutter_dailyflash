import 'package:flutter/material.dart';

class Prob4 extends StatefulWidget {
  const Prob4({super.key});

  @override
  State createState() => _Prob4State();
}

class _Prob4State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash | Prob4"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(flex: 6, child: Container()),
            const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                  Radius.circular(20),
                )),
                hintText: "User Input",
                filled: true,
                fillColor: Color.fromARGB(239, 178, 70, 197),
              ),
            ),
            Expanded(flex: 2, child: Container()),
            ElevatedButton(onPressed: () {}, child: const Text("Submit")),
            Expanded(flex: 6, child: Container())
          ],
        ),
      ),
    );
  }
}
