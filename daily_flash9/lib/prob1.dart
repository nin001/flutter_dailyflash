// 1. Create a ListView but the Listview must be scrollable in a horizontal
// direction. The Listview must contain 5 children. Each child must be a
// Container widget of height 60 and width 60, giving color to the container.

import 'package:flutter/material.dart';

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash | Prob1"),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 60,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  // height: 60,
                  width: 60,
                  color: Colors.amber,
                ),
                const SizedBox(
                  // height: 60,
                  width: 50,
                ),
                Container(
                  // height: 60,
                  width: 60,
                  color: Colors.red,
                ),
                const SizedBox(
                  // height: 60,
                  width: 50,
                ),
                Container(
                  // height: 60,
                  width: 60,
                  color: Colors.blue,
                ),
                const SizedBox(
                  // height: 60,
                  width: 50,
                ),
                Container(
                  // height: 60,
                  width: 60,
                  color: Colors.purple,
                ),
                const SizedBox(
                  // height: 60,
                  width: 50,
                ),
                Container(
                  // height: 60,
                  width: 60,
                  color: Colors.green,
                ),
                const SizedBox(
                  // height: 60,
                  width: 50,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
