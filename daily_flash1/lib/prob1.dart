import 'package:flutter/material.dart';

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.start),
        title: const Text("Title"),
        centerTitle: true,
        actions: const [Icon(Icons.favorite_border_sharp)],
        backgroundColor: Colors.greenAccent,
      ),
    );
  }
}
