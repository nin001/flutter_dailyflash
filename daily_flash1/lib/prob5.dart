import 'package:flutter/material.dart';

class Prob5 extends StatelessWidget {
  const Prob5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Title"),
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
              color: Colors.greenAccent,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                    color: Colors.red, offset: Offset(8, 8), blurRadius: 8)
              ]),
        ),
      ),
    );
  }
}
