import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: const Icon(Icons.agriculture),
          actions: const [
            Icon(Icons.verified_user),
            Icon(Icons.favorite),
            Icon(Icons.abc)
          ],
          backgroundColor: Colors.amber),
    );
  }
}
