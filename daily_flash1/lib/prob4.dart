import 'package:flutter/material.dart';

class Prob4 extends StatelessWidget {
  const Prob4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Title"),
        backgroundColor: Colors.amber,
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
              color: Colors.blue,
              border: Border.all(width: 7, color: Colors.red)),
        ),
      ),
    );
  }
}
