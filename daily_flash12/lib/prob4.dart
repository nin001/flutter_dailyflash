import 'package:flutter/material.dart';

class Prob4 extends StatefulWidget {
  const Prob4({super.key});

  @override
  State<Prob4> createState() => _Prob4State();
}

class _Prob4State extends State<Prob4> {
  TextEditingController name = TextEditingController();
  TextEditingController college = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  List<Model> list = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob4"),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                controller: name,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter your name";
                  } else {
                    return null;
                  }
                },
                decoration: const InputDecoration(
                  label: Text("Enter name"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: college,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter College name";
                  } else {
                    return null;
                  }
                },
                decoration: const InputDecoration(
                  label: Text("Enter College name"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      list.add(
                          Model(name: name.text, collegeName: college.text));
                      setState(() {});
                      name.clear();
                      college.clear();
                    }
                  },
                  child: const Text("Submit")),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: SizedBox(
                  child: ListView.builder(
                      itemCount: list.length,
                      itemBuilder: ((context, index) {
                        return Column(
                          children: [
                            Container(
                              height: 100,
                              width: 600,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 2)),
                              child: Column(
                                children: [
                                  Text("Name : ${list[index].name}"),
                                  Text(
                                      "College-Name : ${list[index].collegeName}")
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            )
                          ],
                        );
                      })),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Model {
  final String name;
  final String collegeName;

  const Model({required this.name, required this.collegeName});
}
