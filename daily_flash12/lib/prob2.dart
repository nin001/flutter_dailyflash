import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Prob2 extends StatefulWidget {
  const Prob2({super.key});

  @override
  State<Prob2> createState() => _Prob2State();
}

class _Prob2State extends State<Prob2> {
  bool tapped = true;

  List<String> list = [];
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob2"),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            TextField(
              controller: controller,
              onSubmitted: (value) {
                list.add(value);
                setState(() {});
                controller.clear();
              },
              decoration: const InputDecoration(
                label: Text("Enter WeekDay"),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
            ),
            SizedBox(
              child: Text(list.toString()),
            )
          ],
        ),
      ),
    );
  }
}
