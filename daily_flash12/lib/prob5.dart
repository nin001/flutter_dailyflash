import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Prob5 extends StatefulWidget {
  const Prob5({super.key});

  @override
  State<Prob5> createState() => _Prob5State();
}

class _Prob5State extends State<Prob5> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob5"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: TextField(
            onTap: () async {
              DateTime? date = await showDatePicker(
                context: context,
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                initialDate: DateTime.now(),
              );

              String dateFormated = DateFormat.yMMMd().format(date!);
              setState(() {
                controller.text = dateFormated;
              });
            },
            controller: controller,
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
            ),
          ),
        ),
      ),
    );
  }
}
