import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Prob3 extends StatefulWidget {
  const Prob3({super.key});

  @override
  State<Prob3> createState() => _Prob3State();
}

class _Prob3State extends State<Prob3> {
  TextEditingController name = TextEditingController();
  TextEditingController college = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob3"),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                controller: name,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter your name";
                  } else {
                    return null;
                  }
                },
                decoration: const InputDecoration(
                  label: Text("Enter name"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: college,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter College name";
                  } else {
                    return null;
                  }
                },
                decoration: const InputDecoration(
                  label: Text("Enter College name"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    formKey.currentState!.validate();
                  },
                  child: const Text("Submit"))
            ],
          ),
        ),
      ),
    );
  }
}
