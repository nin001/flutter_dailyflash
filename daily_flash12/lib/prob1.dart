import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Prob1 extends StatefulWidget {
  const Prob1({super.key});

  @override
  State<Prob1> createState() => _Prob1State();
}

class _Prob1State extends State<Prob1> {
  bool tapped = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob1"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: TextField(
            obscureText: tapped,
            obscuringCharacter: '*',
            decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                suffixIcon: GestureDetector(
                  onTap: () {
                    if (tapped) {
                      tapped = false;
                    } else {
                      tapped = true;
                    }
                    setState(() {});
                  },
                  child: (!tapped)
                      ? const Icon(Icons.remove_red_eye)
                      : const Icon(Icons.visibility_off),
                )),
          ),
        ),
      ),
    );
  }
}
