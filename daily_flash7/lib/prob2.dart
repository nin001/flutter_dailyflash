// Create a Screen that displays an `Icon` widget representing a star the size of the
// icon must be 40 and the color of the icon must be orange, beside the icon place a
// `Text` widget with the content "Rating: 4.5". Apply a font size of 25 and bold
// weight to the text. Refer to below image.

import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob2"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          height: 100,
          width: 200,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.black, width: 1)),
          child: const Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                Icons.star,
                size: 40,
                color: Colors.orange,
              ),
              Text(
                "Rating: 4.5",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              )
            ],
          ),
        ),
      ),
    );
  }
}
