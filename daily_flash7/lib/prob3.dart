// Create a Screen with two horizontally aligned containers at the center of the
// screen. Apply a shadow to each container set individual colors and give a border
// to the Containers only the bottom edges of the container must be rounded.

import 'package:flutter/material.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob3"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(color: Colors.red, boxShadow: [
                BoxShadow(
                    offset: Offset(5, 5),
                    color: Color.fromARGB(255, 240, 127, 119),
                    blurRadius: 8)
              ]),
            ),
            Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(color: Colors.blue, boxShadow: [
                BoxShadow(
                    offset: Offset(5, 5),
                    color: Color.fromARGB(255, 119, 153, 240),
                    blurRadius: 8)
              ]),
            )
          ],
        ),
      ),
    );
  }
}
