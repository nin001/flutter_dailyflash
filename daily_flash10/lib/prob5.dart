import 'package:flutter/material.dart';

class Prob5 extends StatelessWidget {
  const Prob5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob5"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
            height: 100,
            width: 100,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [BoxShadow(offset: Offset(5, 5), color: Colors.red)],
              gradient: LinearGradient(
                  colors: [Colors.blue, Colors.purple, Colors.green],
                  stops: [0.1, 0.4, 0.9]),
            )),
      ),
    );
  }
}
