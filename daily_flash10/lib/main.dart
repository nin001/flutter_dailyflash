import 'package:daily_flash10/prob1.dart';
import 'package:daily_flash10/prob2.dart';
import 'package:daily_flash10/prob3.dart';
import 'package:daily_flash10/prob4.dart';
import 'package:daily_flash10/prob5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(debugShowCheckedModeBanner: false, home: Prob5());
  }
}
