import 'package:flutter/material.dart';

class Prob4 extends StatelessWidget {
  const Prob4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue.shade400,
          title: const Text("DailyFlash"),
        ),
        body: Center(
          child: Container(
            height: 150,
            width: 550,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 1)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 100,
                  width: 150,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 1)),
                  child: Container(
                    height: 70,
                    width: 100,
                    color: Colors.red,
                  ),
                ),
                Container(
                  height: 100,
                  width: 150,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 1)),
                  child: Container(
                    height: 70,
                    width: 100,
                    color: Colors.purple,
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
