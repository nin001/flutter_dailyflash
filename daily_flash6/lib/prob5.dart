// Create a Screen in which we have 3 Containers with size:
// (height:100,width:200) placed vertically. Each container must have a
// black border. Initially, the Color of the Containers must be white. The
// container that is tapped must change its color to red and other containers
// must be white.

import 'package:flutter/material.dart';

class Prob5 extends StatefulWidget {
  const Prob5({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Prob5State();
  }
}

class _Prob5State extends State {
  int pressed = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade400,
        title: const Text("DailyFlash"),
      ),
      backgroundColor: Colors.grey,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {
                pressed = 1;
                setState(() {});
              },
              child: Container(
                height: 100,
                width: 200,
                color: (pressed == 1) ? Colors.red : Colors.white,
              ),
            ),
            GestureDetector(
              onTap: () {
                pressed = 2;
                setState(() {});
              },
              child: Container(
                height: 100,
                width: 200,
                color: (pressed == 2) ? Colors.red : Colors.white,
              ),
            ),
            GestureDetector(
              onTap: () {
                pressed = 3;
                setState(() {});
              },
              child: Container(
                height: 100,
                width: 200,
                color: (pressed == 3) ? Colors.red : Colors.white,
              ),
            ),
            GestureDetector(
              onTap: () {
                pressed = 4;
                setState(() {});
              },
              child: Container(
                height: 100,
                width: 200,
                color: (pressed == 4) ? Colors.red : Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
