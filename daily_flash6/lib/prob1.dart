/*
Create a screen that displays an asset image of the food item at the top of the
Screen, below the image, display the name of the food item and below the name
give the description of the item. Add appropriate padding.*/

import 'package:flutter/material.dart';

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade400,
        title: const Text("DailyFlash"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Image.asset("assets/pizza.jpg"),
            Container(
              color: Colors.white,
              margin: const EdgeInsets.only(top: 20, left: 1, right: 1),
              child: const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Pizza",
                    style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                      "Pizza is a dish of Italian origin consisting of a flattened disk of bread dough topped with a combination of olive oil, oregano, tomato, olives, mozzarella, or other cheese, and other ingredients. It is baked quickly, often commercially, using a wood-fired oven heated to a very high temperature, and served hot.",
                      style: TextStyle(color: Colors.black54))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
