// Create a screen that displays a container. The container must display an image.
// Give a circular border only at the bottom of the container. Below the container
// display the button with size:(width:250, height:70). The button must display
// “Add to cart”. The color of the button must be purple. Both the container and
// button must be in the center of the screen.

import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade400,
        title: const Text("DailyFlash"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 350,
              width: 350,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border(
                      bottom: BorderSide(
                    color: Colors.red,
                    width: 5,
                  )),
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                          "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg"))),
            ),
            GestureDetector(
              onTap: () {
                print("Clicked");
              },
              child: Container(
                width: 250,
                height: 70,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.all(Radius.circular(30))),
                child: const Text(
                  "Add To Cart",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
