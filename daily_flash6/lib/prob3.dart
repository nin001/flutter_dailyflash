import 'package:flutter/material.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade400,
        title: const Text("DailyFlash"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 75,
                width: 75,
                color: Colors.red,
              ),
              Container(
                height: 75,
                width: 75,
                color: Colors.purple,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 75,
                width: 75,
                color: Colors.yellow,
              ),
              Container(
                height: 75,
                width: 75,
                color: Colors.green,
              )
            ],
          ),
        ],
      ),
    );
  }
}
