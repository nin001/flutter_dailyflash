// Create a Screen that displays 3 widgets in a Column. The image must be the
// first widget, the next widget must be a Container of color red and the 3rd
// widget must be a Container of color blue. Place all the 3 widgets in a
// Column.

import 'package:flutter/material.dart';

class Prob5 extends StatelessWidget {
  const Prob5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob5"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.network(
              "https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832_1280.jpg",
              height: 100,
              width: 100,
              fit: BoxFit.fill,
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}
