import 'package:flutter/material.dart';

// 1. Create a Screen, in the appBar display "Profile Information". In the body,
// display an image of size (height: 250 width:250). Below the image add
// appropriate spacing and then display the user Name and Phone Number
// vertically. The name and phone number must have a font size of 16 and a font
// weight of 500.

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Profile Information",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset(
              "nirajPicture.jpg",
              height: 250,
              width: 250,
              fit: BoxFit.fill,
            ),
            const Text(
              "Name : Niraj",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.blueAccent),
            ),
            const Text(
              "Phone No : 8767996709",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.blueAccent),
            ),
          ],
        ),
      ),
    );
  }
}
