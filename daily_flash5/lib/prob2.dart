import 'package:flutter/material.dart';

// Create a Screen in which we have 3 Containers in a Column each container
// must be of height 100 and width 100. Each container must have an image
// as a child.

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob2"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 100,
              width: 100,
              color: Colors.white,
              child: Image.network(
                  "https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832_1280.jpg",
                  fit: BoxFit.fill),
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.white,
              child: Image.network(
                  "https://cdn.pixabay.com/photo/2021/09/20/21/32/lake-6641880_1280.jpg",
                  fit: BoxFit.fill),
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.white,
              child: Image.network(
                  "https://cdn.pixabay.com/photo/2017/01/04/08/27/lake-1951519_960_720.jpg",
                  fit: BoxFit.fill),
            )
          ],
        ),
      ),
    );
  }
}
