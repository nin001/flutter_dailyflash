// Create a Screen and add your image in the center of the screen below your
// image display your name in a container, give a shadow to the Container
// and give a border to the container the top left and top right corners must
// be circular, with a radius of 20. Add appropriate padding to the container.

import 'package:flutter/material.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});

  @override
  Widget build(Object context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob3"),
      ),
      body: Center(
        child: Column(
          children: [
            Image.asset(
              "nirajPicture.jpg",
              height: 500,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(3, 5),
                      color: Color.fromARGB(255, 118, 12, 5),
                      blurRadius: 5)
                ],
                color: Colors.red,
              ),
              child: const Text(
                "Niraj Randhir",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
