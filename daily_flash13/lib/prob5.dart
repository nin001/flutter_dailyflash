// Create a login screen with a username and password and a submit button. The
// username can be anything but it must be greater than 6 characters and not more
// than 20 characters.
// The password must contain at least one Capital letter one small letter a number
// and a special character and the password must be a minimum of 8 characters
// long and a maximum of 20 characters long.
// The user will enter the username and password and press the submit button.
// When The submit button is pressed then validate the TextFormFields and display
// relevant error messages.

import 'package:flutter/material.dart';

class Prob5 extends StatefulWidget {
  const Prob5({super.key});

  @override
  State createState() => _Prob5State();
}

class _Prob5State extends State {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob5"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              TextFormField(
                maxLength: 20,
                decoration: const InputDecoration(
                    labelText: "Enter username",
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.lightGreen),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Mandatory Field";
                  } else if (value.length < 6) {
                    return "Username must be greater than 6letters";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                maxLength: 20,
                decoration: const InputDecoration(
                    labelText: "Enter password",
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.lightGreen),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Mandatory Field";
                  } else if (!(value.contains(RegExp(r'[A-Z]')) &&
                      value.contains(RegExp(r'[a-z]')) &&
                      value.contains(RegExp(r'[0-9]')) &&
                      value.contains(RegExp(r'[!#@$%^&*(),?":{}|<>=]')))) {
                    return "Password must contain At least One (capital letter,small letter,speacial character,number)";
                  } else if (value.length < 8) {
                    return "Password must be greater than 8 letters";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("Successfull login")));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("Unsuccessfull")));
                    }
                  },
                  child: const Text("Login"))
            ],
          ),
        ),
      ),
    );
  }
}
