// Create 2 TextForm fields in which the user will enter the phone number and
// email address. You must validate the text form fields as soon as the user starts
// entering the text in the text form fields and display the error message.

import 'package:flutter/material.dart';

class Prob4 extends StatefulWidget {
  const Prob4({super.key});

  @override
  State<Prob4> createState() => _Prob4State();
}

class _Prob4State extends State<Prob4> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  String? emailErrorText;
  String? phoneErrorText;

  // void checkPhoneError(String value) {
  //   //print("phone error msg");
  //   if (value.isEmpty) {
  //     setState(() {
  //       phoneErrorText = "Enter Phone Number";
  //     });
  //   } else if (value.contains(RegExp(r'[!#$%^&*(),?":{}|<>=]'))) {
  //     setState(() {
  //       phoneErrorText = "Speacial Character not allowed";
  //     });
  //   } else if (value.contains(RegExp(r'[ ]'))) {
  //     setState(() {
  //       phoneErrorText = "No spaces allowed";
  //     });
  //   } else if (!value.endsWith("@gmail.com")) {
  //     setState(() {
  //       phoneErrorText = "Invalid Email Must end with '@gmail.com'";
  //     });
  //   } else {
  //     setState(() {
  //       phoneErrorText = null;
  //     });
  //   }
  // }

  // void checkEmailError(String value) {
  //   if (value.isEmpty) {
  //     emailErrorText = "Enter Phone number , Compulsory field";
  //   } else if (value.contains(RegExp(r'[A-Z]')) ||
  //       value.contains(RegExp(r'[a-z]'))) {
  //     emailErrorText = "No Characters Allowed in Phone Numner";
  //   } else if (value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
  //     emailErrorText = "No speacial character allowed";
  //   } else if (value.length != 10) {
  //     emailErrorText = "Enter Valid phone Number";
  //   } else {
  //     emailErrorText = null;
  //   }
  //   setState(() {});
  // }

  void checkEmailError(String value) {
    if (value.isEmpty) {
      setState(() {
        emailErrorText = "Enter Phone Number";
      });
    } else if (value.contains(RegExp(r'[!#$%^&*(),?":{}|<>=]'))) {
      setState(() {
        emailErrorText = "Special Characters not allowed";
      });
    } else if (value.contains(RegExp(r'[ ]'))) {
      setState(() {
        emailErrorText = "No spaces allowed";
      });
    } else if (!value.endsWith("@gmail.com")) {
      setState(() {
        emailErrorText = "Invalid Email Must end with '@gmail.com'";
      });
    } else {
      setState(() {
        emailErrorText = null;
      });
    }
  }

  void checkPhoneError(String value) {
    if (value.isEmpty) {
      setState(() {
        phoneErrorText = "Enter Phone number , Compulsory field";
      });
    } else if (value.contains(RegExp(r'[A-Z]')) ||
        value.contains(RegExp(r'[a-z]'))) {
      setState(() {
        phoneErrorText = "No Characters Allowed in Phone Number";
      });
    } else if (value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
      setState(() {
        phoneErrorText = "No special character allowed";
      });
    } else if (value.length != 10) {
      setState(() {
        phoneErrorText = "Enter Valid phone Number";
      });
    } else {
      setState(() {
        phoneErrorText = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Prob4"), backgroundColor: Colors.blue),
      body: Form(
        key: formKey,
        child: Column(
          children: [
            TextFormField(
              controller: phoneController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(), hintText: "Enter Phone Number"),
              // validator: (value) {
              //   return phoneErrorText;
              // },
              onChanged: (value) {
                checkPhoneError(value);
              },
            ),
            if (phoneErrorText != null)
              Text(
                phoneErrorText!,
                style: const TextStyle(color: Colors.red),
              ),
            TextFormField(
              controller: emailController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(), hintText: "Enter Email ID"),
              // validator: (value) {
              //   return emailErrorText;
              // },
              onChanged: checkEmailError,
            ),
            if (emailErrorText != null)
              Text(
                emailErrorText!,
                style: const TextStyle(color: Colors.red),
              ),
          ],
        ),
      ),
    );
  }
}
