// Create a Flutter screen in which you will take user input using TextFormField.
// The user must enter a phone number and also give a submit button below the text
// form field.
// When the submit button is pressed check whether the user has entered a valid
// phone number i.e. the phone number must be 10 digits long, and the phone
// number must not contain any characters or special characters, and display the
// error message accordingly.

import 'package:flutter/material.dart';

class Prob2 extends StatefulWidget {
  const Prob2({super.key});

  @override
  State<Prob2> createState() => _Prob2State();
}

class _Prob2State extends State<Prob2> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool showContainer = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Prob1"), backgroundColor: Colors.blue),
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              const Expanded(flex: 1, child: Text("Enter Phone Number")),
              Expanded(
                flex: 1,
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: "Enter Phone Number",
                      hintStyle: const TextStyle(
                          fontWeight: FontWeight.w300, color: Colors.black38),
                      filled: true,
                      fillColor: Colors.green.shade200),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter Phone number , Compulsory field";
                    } else if (value.contains(RegExp(r'[A-Z]')) ||
                        value.contains(RegExp(r'[a-z]'))) {
                      return "No Characters Allowed in Phone Numner";
                    } else if (value
                        .contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
                      return "No speacial character allowed";
                    } else if (value.length != 10) {
                      return "Enter Valid phone Number";
                    }
                    return null;
                  },
                ),
              ),
              Expanded(
                flex: 1,
                child: ElevatedButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        showContainer = true;
                        setState(() {});
                      } else {
                        showContainer = false;
                      }
                    },
                    child: const Text("Submit")),
              ),
              Expanded(flex: 1, child: Container()),
              (showContainer)
                  ? Expanded(
                      flex: 2,
                      child: Container(
                        height: 200,
                        width: 600,
                        alignment: Alignment.center,
                        color: Colors.greenAccent,
                        child: const Text("Submitted"),
                      ),
                    )
                  : Expanded(flex: 2, child: Container())
            ],
          ),
        ),
      ),
    );
  }
}
