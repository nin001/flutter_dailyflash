// Create a Flutter screen in which you must have 2 TextFormFields and a submit
// button.
// You will have to validate the TextFormFields when the submit button is pressed
// i.e. check whether the user has entered any data in the TextFormFields. If no data
// is entered display an error message.

import 'package:flutter/material.dart';

class Prob1 extends StatefulWidget {
  const Prob1({super.key});

  @override
  State<Prob1> createState() => _Prob1State();
}

class _Prob1State extends State<Prob1> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool showContainer = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Prob1"), backgroundColor: Colors.blue),
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              Expanded(
                flex: 3,
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: "Enter Text",
                      hintStyle: const TextStyle(
                          fontWeight: FontWeight.w300, color: Colors.black38),
                      filled: true,
                      fillColor: Colors.green.shade200),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter Value In TextField 1";
                    }
                    return null;
                  },
                ),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 3,
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: "Enter Text",
                      hintStyle: const TextStyle(
                          fontWeight: FontWeight.w300, color: Colors.black38),
                      filled: true,
                      fillColor: Colors.green.shade200),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter Value In TextField 2";
                    }
                    return null;
                  },
                ),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 2,
                child: ElevatedButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        showContainer = true;
                        setState(() {});
                      } else {
                        showContainer = false;
                      }
                    },
                    child: const Text("Submit")),
              ),
              Expanded(flex: 1, child: Container()),
              (showContainer)
                  ? Expanded(
                      flex: 7,
                      child: Container(
                        height: 200,
                        width: 600,
                        alignment: Alignment.center,
                        color: Colors.greenAccent,
                        child: const Text("Submitted"),
                      ),
                    )
                  : Expanded(flex: 6, child: Container())
            ],
          ),
        ),
      ),
    );
  }
}
