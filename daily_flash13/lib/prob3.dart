// Create a screen displaying a TextFormField where the user will enter an email id.
// Below the text form field display a submit button. When the button is pressed
// validate the email address entered in the text form field. You must check whether
// the email address ends with @gmail.com. The email address must not contain
// any spaces.

import 'package:flutter/material.dart';

class Prob3 extends StatefulWidget {
  const Prob3({super.key});

  @override
  State<Prob3> createState() => _Prob3State();
}

class _Prob3State extends State<Prob3> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool showContainer = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Prob3"), backgroundColor: Colors.blue),
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              const Expanded(flex: 1, child: Text("Enter Email address")),
              Expanded(
                flex: 1,
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: "Enter Email-Id",
                      hintStyle: const TextStyle(
                          fontWeight: FontWeight.w300, color: Colors.black38),
                      filled: true,
                      fillColor: Colors.green.shade200),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter Email-Id , Compulsory field";
                    } else if (value
                        .contains(RegExp(r'[!#$%^&*(),?":{}|<>=]'))) {
                      return "Speacial Character not allowed";
                    } else if (value.contains(RegExp(r'[ ]'))) {
                      return "No spaces allowed";
                    } else if (!value.endsWith("@gmail.com")) {
                      return "Invalid Email Must end with '@gmail.com'";
                    }
                    return null;
                  },
                ),
              ),
              Expanded(
                flex: 1,
                child: ElevatedButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        showContainer = true;
                        setState(() {});
                      } else {
                        showContainer = false;
                      }
                    },
                    child: const Text("Submit")),
              ),
              Expanded(flex: 1, child: Container()),
              (showContainer)
                  ? Expanded(
                      flex: 2,
                      child: Container(
                        height: 200,
                        width: 600,
                        alignment: Alignment.center,
                        color: Colors.greenAccent,
                        child: const Text("Submitted"),
                      ),
                    )
                  : Expanded(flex: 2, child: Container())
            ],
          ),
        ),
      ),
    );
  }
}
