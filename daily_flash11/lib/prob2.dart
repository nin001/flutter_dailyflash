import 'package:flutter/material.dart';

class Prob2 extends StatefulWidget {
  const Prob2({super.key});

  @override
  State<Prob2> createState() => _Prob2State();
}

class _Prob2State extends State<Prob2> {
  bool tapped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob2"),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(50),
        child: Center(
          child: TextField(
            onTap: () {
              setState(() {
                if (tapped) {
                  tapped = false;
                } else {
                  tapped = true;
                }
              });
            },
            decoration: InputDecoration(
                border: const OutlineInputBorder(),
                suffixIcon: SizedBox(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      (tapped) ? const Icon(Icons.search) : Container(),
                      const Icon(Icons.lock)
                    ],
                  ),
                )),
          ),
        ),
      ),
    );
  }
}
