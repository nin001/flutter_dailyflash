import 'package:flutter/material.dart';

class Prob5 extends StatelessWidget {
  const Prob5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob5"),
        backgroundColor: Colors.blue,
      ),
      body: const Padding(
        padding: EdgeInsets.all(50),
        child: Center(
          child: TextField(
            maxLines: 7 /*Thala for a reason*/,
            decoration: InputDecoration(
              label: Text("Enter your name"),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
            ),
          ),
        ),
      ),
    );
  }
}
