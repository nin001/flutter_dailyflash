import 'package:flutter/material.dart';

class Prob5 extends StatelessWidget {
  const Prob5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Problem 5",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black,
      ),
      body: Center(
          child: Container(
        height: 200,
        width: 200,
        decoration: const BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
                colors: [Colors.red, Colors.red, Colors.green, Colors.green],
                stops: [0.0, 0.499, 0.501, 1.0],
                transform: GradientRotation(0))),
      )),
    );
  }
}
