import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Problem 2",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      "https://cdn.pixabay.com/photo/2016/05/05/02/37/sunset-1373171_1280.jpg"),
                  fit: BoxFit.fill)),
          child: const Text(
            "Core2Web",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
