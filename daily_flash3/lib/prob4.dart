import 'package:flutter/material.dart';

class Prob4 extends StatelessWidget {
  const Prob4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Problem 4",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(color: Colors.cyanAccent, boxShadow: [
            BoxShadow(
                offset: Offset(0, -20), color: Colors.black, blurRadius: 3)
          ]),
        ),
      ),
    );
  }
}
