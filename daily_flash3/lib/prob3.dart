import 'package:flutter/material.dart';

class Prob3 extends StatefulWidget {
  const Prob3({super.key});

  @override
  State createState() => _Prob3State();
}

class _Prob3State extends State {
  bool tap = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Problem 3",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              if (tap) {
                tap = false;
              } else {
                tap = true;
              }
            });
          },
          child: Container(
            height: 200,
            width: 200,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.cyan,
                border: Border.all(
                    width: 7, color: (tap) ? Colors.green : Colors.red)),
            child: const Text("Tap On Me"),
          ),
        ),
      ),
    );
  }
}
