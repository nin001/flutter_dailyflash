import 'package:flutter/material.dart';

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Problem 1",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          color: Colors.greenAccent,
          //margin: const EdgeInsets.all(70),
          alignment: Alignment.center,
          child: Image.network(
            height: 200,
            width: 200,
            "https://cdn.pixabay.com/photo/2018/03/27/21/43/startup-3267505_640.jpg",
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
