import 'package:flutter/material.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Prob 3"),
          backgroundColor: Colors.white,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: const Row(
            children: [
              Text("Niraj"),
              Icon(Icons.settings_input_component_rounded)
            ],
          ),
        ));
  }
}
