import 'package:flutter/material.dart';

class Prob5 extends StatefulWidget {
  const Prob5({super.key});

  @override
  State<Prob5> createState() => _Prob5State();
}

class _Prob5State extends State<Prob5> {
  Color color = Colors.green;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob 5"),
        backgroundColor: Colors.white,
      ),
      floatingActionButton: GestureDetector(
        onLongPress: () {
          color = Colors.purple;
          setState(() {});
        },
        child: FloatingActionButton(
            backgroundColor: color,
            onPressed: () {},
            child: const Icon(Icons.abc)),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
