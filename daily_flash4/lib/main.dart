// import 'package:daily_flash4/prob1.dart';
// import 'package:daily_flash4/prob2.dart';
// import 'package:daily_flash4/prob3.dart';
// import 'package:daily_flash4/prob4.dart';
import 'package:daily_flash4/prob5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Prob5(),
    );
  }
}
