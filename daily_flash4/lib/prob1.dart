import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Prob 1",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {},
          style: const ButtonStyle(
              fixedSize: MaterialStatePropertyAll(Size(150, 70)),
              // side: MaterialStatePropertyAll(BorderSide(width: 5)),
              // shape: MaterialStatePropertyAll(),
              shadowColor: MaterialStatePropertyAll(Colors.red)),
          child: const Text("Elevated Button"),
        ),
      ),
    );
  }
}
