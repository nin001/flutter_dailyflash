import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Prob 2",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {},
          style: const ButtonStyle(
              fixedSize: MaterialStatePropertyAll(Size(200, 200)),
              side: MaterialStatePropertyAll(
                  BorderSide(width: 5, color: Colors.red)),
              shape: MaterialStatePropertyAll(CircleBorder()),
              shadowColor: MaterialStatePropertyAll(Colors.red)),
          child: const Icon(Icons.ac_unit_outlined),
        ),
      ),
    );
  }
}
