import 'package:flutter/material.dart';

class Prob4 extends StatelessWidget {
  const Prob4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Prob 4"),
          backgroundColor: Colors.white,
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {},
            hoverColor: Colors.orange,
            child: const Icon(Icons.abc)));
  }
}
