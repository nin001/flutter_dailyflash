import 'package:flutter/material.dart';

class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: const [
          Icon(
            Icons.run_circle,
            size: 50,
          )
        ],
        bottom: PreferredSize(
            preferredSize: const Size.fromHeight(4),
            child: Container(
              height: 2,
              color: Colors.black,
            )),
      ),
      body: Padding(
        padding: const EdgeInsets.all(40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    flex: 3,
                    child: Container(
                      height: 150,
                      color: Colors.amber,
                    )),
                const Expanded(
                    flex: 1,
                    child: SizedBox(
                      height: 150,
                    )),
                Expanded(
                    flex: 3,
                    child: Container(
                      height: 150,
                      color: Colors.red,
                    )),
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 150,
                    color: Colors.green,
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    flex: 3,
                    child: Container(
                      height: 150,
                      color: Colors.purple,
                    )),
                const Expanded(
                    flex: 1,
                    child: SizedBox(
                      height: 150,
                    )),
                Expanded(
                    flex: 3,
                    child: Container(
                      height: 150,
                      color: Colors.blue,
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
