import 'package:flutter/material.dart';

class Prob4 extends StatelessWidget {
  const Prob4({super.key});

  final List<Model> list = const [
    Model(
        img:
            "https://cdn.pixabay.com/photo/2020/06/13/17/51/milky-way-5295160_1280.jpg",
        name: "Night Sky"),
    Model(
        img:
            "https://cdn.pixabay.com/photo/2016/11/29/12/22/stars-1869447_1280.jpg",
        name: "Green Night"),
    Model(
        img:
            "https://cdn.pixabay.com/photo/2020/11/14/17/50/night-sky-5742416_1280.jpg",
        name: "Mountain"),
    Model(
        img:
            "https://cdn.pixabay.com/photo/2016/11/25/23/15/moon-1859616_1280.jpg",
        name: "Moon"),
    Model(
        img:
            "https://cdn.pixabay.com/photo/2018/03/02/19/21/nature-3194001_1280.jpg",
        name: "Forest")
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob 4"),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.black),
                      borderRadius: BorderRadius.circular(20)),
                  child: Row(children: [
                    Expanded(
                      flex: 2,
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(list[index].img),
                                fit: BoxFit.fill)),
                      ),
                    ),
                    Expanded(
                        flex: 6,
                        child: Text(
                          list[index].name,
                          textAlign: TextAlign.center,
                        ))
                  ]),
                ),
                const SizedBox(
                  height: 20,
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

class Model {
  final String img;
  final String name;

  const Model({required this.img, required this.name});
}
