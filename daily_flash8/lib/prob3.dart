import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob 3"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          height: 100,
          width: 300,
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.black),
            borderRadius: const BorderRadius.all(Radius.circular(30)),
          ),
          child: Row(
            children: [
              Expanded(
                  flex: 100,
                  child: Container(
                    height: 100,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: 100,
                    color: Colors.black,
                  )),
              Expanded(
                  flex: 100,
                  child: Container(
                    height: 100,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: 100,
                    color: Colors.black,
                  )),
              Expanded(
                  flex: 100,
                  child: Container(
                    height: 100,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
