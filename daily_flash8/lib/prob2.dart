import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob2"),
        backgroundColor: Colors.blueAccent,
      ),
      body: ListView(scrollDirection: Axis.horizontal, children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.white70,
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn.pixabay.com/photo/2014/11/05/15/57/salmon-518032_1280.jpg",
                      height: 200,
                      width: 150,
                      fit: BoxFit.fill,
                    ),
                    const Text("Food 1",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.purpleAccent,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.white70,
                child: Column(
                  children: [
                    Image.network(
                        "https://cdn.pixabay.com/photo/2015/04/08/13/13/food-712665_1280.jpg",
                        height: 200,
                        width: 150,
                        fit: BoxFit.fill),
                    const Text("Food 2",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.purpleAccent,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.white70,
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn.pixabay.com/photo/2010/12/13/10/05/berries-2277_1280.jpg",
                      height: 200,
                      width: 150,
                      fit: BoxFit.fill,
                    ),
                    const Text("Food 3",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.purpleAccent,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.white70,
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn.pixabay.com/photo/2017/05/07/08/56/pancakes-2291908_1280.jpg",
                      height: 200,
                      width: 150,
                      fit: BoxFit.fill,
                    ),
                    const Text("Food 4",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.purpleAccent,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.white70,
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn.pixabay.com/photo/2017/01/30/13/49/pancakes-2020863_1280.jpg",
                      height: 200,
                      width: 150,
                      fit: BoxFit.fill,
                    ),
                    const Text("Food 5",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.purpleAccent,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}
