import 'package:flutter/material.dart';

// container of 200*200 at center with red border and text present in the center of the container
class Prob1 extends StatelessWidget {
  const Prob1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Prob1"),
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          alignment: Alignment
              .center, // This brings the text at the center // alignment property which represents the alignment of the child withing the contianer
          decoration: BoxDecoration(
              color: Colors.greenAccent,
              border: Border.all(color: Colors.red, width: 10),
              borderRadius: const BorderRadius.all(Radius.circular(20))),
          child: const Text(
            "Text at the center",
          ),
        ),
      ),
    );
  }
}
