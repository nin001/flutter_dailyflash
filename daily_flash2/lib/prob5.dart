import 'package:flutter/material.dart';

class Prob5 extends StatefulWidget {
  const Prob5({super.key});

  @override
  State<Prob5> createState() => _Prob5State();
}

class _Prob5State extends State<Prob5> {
  bool clicked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Prob5")),
        body: Center(
          child: GestureDetector(
            onTap: () {
              setState(() {
                if (clicked) {
                  clicked = false;
                } else {
                  clicked = true;
                }
              });
            },
            child: Container(
              height: 60,
              width: 200,
              padding: const EdgeInsets.only(top: 5, left: 10),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: Colors.red, width: 3),
                  color: (!clicked)
                      ? const Color.fromARGB(133, 255, 82, 82)
                      : Colors.blue),
              child: (!clicked)
                  ? const Text("ClickMe")
                  : const Text("Cotainer Clicked"),
            ),
          ),
        ));
  }
}
