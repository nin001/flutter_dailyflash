import 'package:flutter/material.dart';

class Prob3 extends StatelessWidget {
  const Prob3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Prob3")),
        body: Center(
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                borderRadius:
                    const BorderRadius.only(topRight: Radius.circular(20)),
                color: Colors.purpleAccent,
                border: Border.all(color: Colors.deepPurple, width: 3)),
          ),
        ));
  }
}
