import 'package:flutter/material.dart';

class Prob4 extends StatelessWidget {
  const Prob4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Prob4")),
        body: Center(
          child: Container(
            height: 60,
            width: 200,
            padding: const EdgeInsets.only(top: 5, left: 10),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                border: Border.all(color: Colors.red, width: 3),
                color: const Color.fromARGB(133, 255, 82, 82)),
            child: const Text("Niraj Randhir"),
          ),
        ));
  }
}
