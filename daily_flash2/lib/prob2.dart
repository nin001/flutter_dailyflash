import 'package:flutter/material.dart';

class Prob2 extends StatelessWidget {
  const Prob2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Prob2 ")),
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          padding: const EdgeInsets.symmetric(vertical: 10),
          decoration: const BoxDecoration(
            border:
                Border(left: BorderSide(color: Colors.purpleAccent, width: 5)),
            color: Colors.blueGrey,
          ),
          child: const Text("Text"),
        ),
      ),
    );
  }
}
